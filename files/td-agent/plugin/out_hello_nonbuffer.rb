require "fluent/plugin/output"

module Fluent::Plugin
  class HelloNonbufferOutput < Output
    Fluent::Plugin.register_output('hello_nonbuffer', self)

    def initialize
      super
      log.debug "--- initialize"
    end

    def configure(conf)
      super
      log.debug "--- configure"
    end

    def start
      super
      log.debug "--- start"
    end

    def shutdown
      super
      log.debug "--- shutdown"
    end

    def format(tag, time, record)
      log.debug "--- format"
    end

    def process(tag, es)
      log.debug "--- process -------------------------"
    end

  end
end
