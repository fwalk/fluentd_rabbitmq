# 
# /opt/td-agent/embedded/lib/ruby/gems/2.5.0/gems/fluent-plugin-rabbitmq-0.0.4/lib/fluent/plugin/out_rabbitmq.rb
# 

require 'fluent/plugin/output'
require 'msgpack'

module Fluent
  module Plugin
    class RabbitMQTestOutput < Output

      Fluent::Plugin.register_output('rtest', self)

      helpers :thread

      def initialize
        super
        require "bunny"
      end

      def configure(conf)
        super
        bunny_options = {}
        bunny_options[:host] = "127.0.0.1"
        bunny_options[:port] = "5672"
        bunny_options[:user] = "rabbitmq"
        bunny_options[:pass] = "rabbitmq"
        bunny_options[:vhost] = "/"
        @bunny = Bunny.new(bunny_options)
      end

      # def multi_workers_ready?
      #   true
      # end      

      def start
        super
        log.debug "--- start"
        @bunny.start
        channel = @bunny.create_channel
        @queue  = channel.queue("rabbitmqtest.hello_world", :auto_delete => false)
        @exchange = channel.default_exchange        
        @exchange.publish("Helloooo! start", :routing_key => @queue.name)          
        log.debug "=== start"
      end

      def shutdown
        @bunny.close
        super
      end     

      def process(tag, es)
        log.debug "--- process"
        log.debug "=== process"
      end

      # def write(chunk)
      #   log.debug "--- write"
      #   log.debug "=== write"
      # end

      # def try_write(chunk)
      #   log.debug "--- try_write"
      #   log.debug "=== try_write"
      # end

      # def process(tag, es)
      #   puts "process - begin"
      #   es.each do |time, record|
      #     # @publish_options[:timestamp] = time.to_i if @timestamp
      #     # formatted = format(tag, time, record)
      #     # @bunny_exchange.publish(formatted, @publish_options)

      #     @exchange.publish("Helloooo!", :routing_key => @queue.name)          
      #     puts "process - end"
      #   end  
      # end      

    end
  end
end

