
Overview

```plain
                   --- fluent-bit ---+
                                     |    :24224
   cpu (ok) ------o--- fluent-bit ---o--- fluentd ---o--- rabbitmq (fluent-plugin-rabbitmq)
   stdin (??) ----+                                  +--- elasticsearch
   journald (??) -+                                
```

Forward from fluent-bit to fluentd

- https://github.com/fluent/fluent-bit-docs/blob/master/output/forward.md

=/etc/td-agent/td-agent.conf=

```plain
<source>
  type forward
  bind 0.0.0.0
  port 24224
</source>

<match **>
  type stdout
</match>
```

=/etc/td-agent-bit/td-agent-bit.conf=

```plain
...
[OUTPUT]
    Name          forward
    Match         stdin
    Host          127.0.0.1
    Port          24224

```

# References

## journal remote 

- https://www.freedesktop.org/wiki/Software/systemd/export/
- https://fedora.pkgs.org/26/fedora-x86_64/systemd-journal-remote-233-6.fc26.x86_64.rpm.html
- Centos7: systemd-journal-gateway.x86_64 : Gateway for serving journal events over the network using : HTTP

## fluentd

- https://github.com/One-com/fluent-plugin-journal-parser
- https://docs.fluentd.org/v0.12/articles/install-by-rpm
- https://github.com/nttcom/fluent-plugin-rabbitmq

```plain
# yum install systemd-journal-gateway

curl -L https://toolbelt.treasuredata.com/sh/install-redhat-td-agent2.5.sh | sh
```

```plain
/opt/td-agent/embedded/bin/fluent-gem  install fluent-plugin-rabbitmq
...
Done installing documentation for serverengine, strptime, dig_rb, fluentd, amq-protocol, bunny, fluent-plugin-rabbitmq after 6 seconds
```

```plain
curl -X POST -d 'json={"json":"message"}' http://localhost:8888/debug.test
```

```plain
tail -f /var/log/td-agent/td-agent.log
```
VERBOSE

```plain
/opt/td-agent/embedded/bin/fluentd -c td-agent.conf -p plugin -vvv
```

## fluentbit

- https://fluentbit.io/documentation/current/about/fluentd_and_fluentbit.html
- https://fluentbit.io/documentation/current/installation/build_install.html
- https://fluentbit.io/documentation/current/installation/td-agent-bit.html
- https://fluentbit.io/documentation/current/installation/redhat_centos.html
- https://fluentbit.io/documentation/current/input/
- https://fluentbit.io/documentation/current/input/systemd.html
- https://fluentbit.io/documentation/current/output/


```plain
echo "test1" | /usr/local/bin/td-agent-bit -i stdin -o forward
```

## rabbit

```plain
rabbitmqadmin -u rabbitmq -p rabbitmq publish routing_key=test1 payload="hello, world2"
```

## ruby

```plain
gem install bunny
...
        amq-protocol requires Ruby version >= 2.2.
```
- https://www.linuxhelp.com/how-to-install-ruby-2-2-on-centos-7/


