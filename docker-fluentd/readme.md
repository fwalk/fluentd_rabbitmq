# Build

Build with default tag ("latest"):

```plain
make clean all
```

Build with given tag:

```plain
make all -e version=1.0.0
```